const csv = require('csv-parser')
const fs = require('fs')
const NumberOfRegistrationsPerYear = {};
fs.createReadStream('csv/company_master_data_upto_Mar_2015_Maharashtra.csv','utf-8')
  .pipe(csv())
  .on('data', (data) => {

    const date=data.DATE_OF_REGISTRATION.split('-');
    if (date[2] >= 1980 && date[2] <= 2018) {
        if (!NumberOfRegistrationsPerYear[date[2]]) {
          NumberOfRegistrationsPerYear[date[2]] = 1;
        } else {
          NumberOfRegistrationsPerYear[date[2]] += 1;
        }
      }
  
  })
  .on('end', () => {
    fs.writeFile('prob2.json', JSON.stringify(NumberOfRegistrationsPerYear), () => {});
  })