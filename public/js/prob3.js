const csv = require('csv-parser')
const fs = require('fs')
const PBARegistrations2015 = {};
fs.createReadStream('csv/company_master_data_upto_Mar_2015_Maharashtra.csv','utf-8')
  .pipe(csv())
  .on('data', (data) => {

    const date=data.DATE_OF_REGISTRATION.split('-');
    if(date[2]==2015)
    {
      if (!PBARegistrations2015[data.PRINCIPAL_BUSINESS_ACTIVITY]) {
        PBARegistrations2015[data.PRINCIPAL_BUSINESS_ACTIVITY] = 1;
    } else {
        PBARegistrations2015[data.PRINCIPAL_BUSINESS_ACTIVITY] += 1;
          }

    }
  })
  .on('end', () => {
    fs.writeFile('prob3.json', JSON.stringify(PBARegistrations2015), () => {});
  })