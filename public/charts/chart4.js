fetch('./prob4.json')
  .then(resp => resp.json())
  .then((data) => {
    const year = [2010];
    for (let i = 0; year[i] < 2019; i += 1) { // stores related year
      year.push(1 + year[i]);
    }
    const series_array = []; //Array input
    const pba = Object.keys(data); // Array for looping 
    for (let i = 0; i < pba.length; i++) {
      series_array.push(
        {
          name: pba[i],                      // Name of  series
          data: Object.values(data[pba[i]]), // Corresponding values of each Principal Business Activity.
        },
      );
    }
    Highcharts.chart('container4', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Stacked-bar-chart(Aggregated number of company registrations of all Principal Business Activities per year)',
      },
      xAxis: {
        title: {
          text: '<---- Year ---->',
        },
        categories: year, // Required years(2010 to 2018) for plotting
      },
      yAxis: {
        title: {
          text: '<---- Number of Company Registraions ---->',
        },
      },
      plotOptions: {
        series: {
          stacking: 'normal',
        },
      },
      series: series_array,
    });
  });
